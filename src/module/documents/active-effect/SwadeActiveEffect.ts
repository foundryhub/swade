import { DocumentModificationOptions } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/document.mjs';
import {
  ActiveEffectDataConstructorData,
  ActiveEffectDataProperties,
} from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/activeEffectData';
import { EffectChangeData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/effectChangeData';
import { BaseUser } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/documents.mjs';
import { PropertiesToSource } from '@league-of-foundry-developers/foundry-vtt-types/src/types/helperTypes';
import { constants } from '../../constants';
import { isFirstOwner } from '../../util';
import SwadeActor from '../actor/SwadeActor';
import SwadeItem from '../item/SwadeItem';

declare global {
  interface DocumentClassConfig {
    ActiveEffect: typeof SwadeActiveEffect;
  }
  interface FlagConfig {
    ActiveEffect: {
      swade: {
        removeEffect?: boolean;
        expiration?: number;
        loseTurnOnHold?: boolean;
        favorite?: boolean;
        related?: Record<string, ActiveEffectDataConstructorData>;
      };
    };
  }
}

export default class SwadeActiveEffect extends ActiveEffect {
  get affectsItems() {
    if (this.parent instanceof CONFIG.Actor.documentClass) {
      const affectedItems = new Array<SwadeItem>();
      this.changes.forEach((c) =>
        affectedItems.push(
          ...this._getAffectedItems(this.parent as SwadeActor, c),
        ),
      );
      return affectedItems.length > 0;
    }
    return false;
  }

  get statusId() {
    return this.getFlag('core', 'statusId');
  }

  get expiresAtStartOfTurn(): boolean {
    const expiration = this.getFlag('swade', 'expiration') ?? -1;
    return [
      constants.STATUS_EFFECT_EXPIRATION.StartOfTurnAuto,
      constants.STATUS_EFFECT_EXPIRATION.StartOfTurnPrompt,
    ].includes(expiration);
  }

  get expiresAtEndOfTurn(): boolean {
    const expiration = this.getFlag('swade', 'expiration') ?? -1;
    return [
      constants.STATUS_EFFECT_EXPIRATION.EndOfTurnAuto,
      constants.STATUS_EFFECT_EXPIRATION.EndOfTurnPrompt,
    ].includes(expiration);
  }

  static ITEM_REGEXP = /@([a-zA-Z0-9]+)\{(.+)\}\[([\S.]+)\]/;

  static override migrateData(data) {
    super.migrateData(data);
    if ('changes' in data) {
      for (const change of data.changes) {
        const match: RegExpMatchArray = change.key.match(
          SwadeActiveEffect.ITEM_REGEXP,
        );
        if (match) {
          const newKey = match[3].trim().replace(/^data\./, 'system.');
          change.key = `@${match[1].trim()}{${match[2].trim()}}[${newKey}]`;
        }
      }
    }
    return data;
  }

  override apply(actor: SwadeActor, change: EffectChangeData) {
    const match = change.key.match(SwadeActiveEffect.ITEM_REGEXP);
    if (match) {
      //get the properties from the match
      const key = match[3].trim();
      const value = change.value;
      //get the affected items
      const affectedItems = this._getAffectedItems(actor, change);
      //apply the AE to each item
      for (const item of affectedItems) {
        const overrides = foundry.utils.flattenObject(item.overrides);
        overrides[key] = Number.isNumeric(value) ? Number(value) : value;
        //mock up a new change object with the key and value we extracted from the original key and feed it into the super apply method alongside the item
        const mockChange = { ...change, key, value };
        //@ts-expect-error It normally expects an Actor but since it only targets the data we can re-use it for Items
        super.apply(item, mockChange);
        item.overrides = foundry.utils.expandObject(overrides);
      }
    } else {
      return super.apply(actor, change);
    }
  }

  private _getAffectedItems(actor: SwadeActor, change: EffectChangeData) {
    const items = new Array<SwadeItem>();
    const match = change.key.match(SwadeActiveEffect.ITEM_REGEXP);
    if (match) {
      //get the properties from the match
      const type = match[1].trim().toLowerCase();
      const name = match[2].trim();
      //filter the items down, according to type and name/id
      items.push(
        ...actor.items.filter(
          (i) => i.type === type && (i.name === name || i.id === name),
        ),
      );
    }
    return items;
  }

  /**
   * Removes Effects from Items
   * @param parent The parent object
   */
  private _removeEffectsFromItems(parent: SwadeActor) {
    const affectedItems = new Array<SwadeItem>();
    this.changes.forEach((c) =>
      affectedItems.push(...this._getAffectedItems(parent, c)),
    );
    for (const item of affectedItems) {
      const overrides = foundry.utils.flattenObject(item.overrides);
      for (const change of this.changes) {
        const match = change.key.match(SwadeActiveEffect.ITEM_REGEXP);
        if (match) {
          const key = match[3].trim();
          //delete override
          delete overrides[key];
          //restore original data from source
          const source = getProperty(item._source, key);
          setProperty(item, key, source);
        }
      }
      item.overrides = foundry.utils.expandObject(overrides);
      if (item.sheet?.rendered) item.sheet.render(true);
    }
  }

  /** This functions checks the effect expiration behavior and either auto-deletes or prompts for deletion */
  async expire() {
    if (!isFirstOwner(this.parent)) {
      return game.swade.sockets.removeStatusEffect(this.uuid);
    }

    const statusId = this.getFlag('core', 'statusId') ?? '';
    if (game.swade.effectCallbacks.has(statusId)) {
      const callbackFn = game.swade.effectCallbacks.get(statusId, {
        strict: true,
      });
      return callbackFn(this);
    }

    const expiration = this.getFlag('swade', 'expiration');
    const startOfTurnAuto =
      expiration === constants.STATUS_EFFECT_EXPIRATION.StartOfTurnAuto;
    const startOfTurnPrompt =
      expiration === constants.STATUS_EFFECT_EXPIRATION.StartOfTurnPrompt;
    const endOfTurnAuto =
      expiration === constants.STATUS_EFFECT_EXPIRATION.EndOfTurnAuto;
    const endOfTurnPrompt =
      expiration === constants.STATUS_EFFECT_EXPIRATION.EndOfTurnPrompt;
    const auto = startOfTurnAuto || endOfTurnAuto;
    const prompt = startOfTurnPrompt || endOfTurnPrompt;

    if (auto) {
      await this.delete();
    } else if (prompt) {
      this.promptEffectDeletion();
    }
  }

  isExpired(pointInTurn: 'start' | 'end'): boolean {
    const isRightPointInTurn =
      (pointInTurn === 'start' && this.expiresAtStartOfTurn) ||
      (pointInTurn === 'end' && this.expiresAtEndOfTurn);
    const remaining = this.duration.remaining ?? 0;
    return isRightPointInTurn && remaining < 1;
  }

  /** @deprecated */
  async removeEffect() {
    await this.expire();
  }

  async promptEffectDeletion() {
    const title = game.i18n.format('SWADE.RemoveEffectTitle', {
      label: this.label,
    });
    const content = game.i18n.format('SWADE.RemoveEffectBody', {
      label: this.label,
      parent: this.parent?.name,
    });
    const buttons: Record<string, Dialog.Button> = {
      yes: {
        label: game.i18n.localize('Yes'),
        icon: '<i class="fas fa-check"></i>',
        callback: () => this.delete(),
      },
      no: {
        label: game.i18n.localize('No'),
        icon: '<i class="fas fa-times"></i>',
      },
      reset: {
        label: game.i18n.localize('SWADE.ActiveEffects.ResetDuration'),
        icon: '<i class="fas fa-repeat"></i>',
        callback: async () => {
          await this.resetDuration();
        },
      },
    };
    new Dialog({ title, content, buttons }).render(true);
  }

  async resetDuration() {
    await this.update({
      duration: {
        startRound: game.combat?.round ?? 1,
        startTime: game.time.worldTime,
      },
    });
  }

  /** A shortcut to make the function public */
  getSourceName = this._getSourceName;

  protected override async _onUpdate(
    changed: PropertiesToSource<ActiveEffectDataProperties>,
    options: DocumentModificationOptions,
    userId: string,
  ) {
    await super._onUpdate(changed, options, userId);
    if (this.getFlag('swade', 'loseTurnOnHold')) {
      const combatant = game.combat?.combatants.find(
        (c) => c.actor?.id === this.parent?.id,
      );
      if (combatant?.getFlag('swade', 'roundHeld')) {
        await combatant?.setFlag('swade', 'turnLost', true);
        await combatant?.unsetFlag('swade', 'roundHeld');
      }
    }
  }

  protected override async _preUpdate(
    changed: ActiveEffectDataConstructorData,
    options: DocumentModificationOptions,
    user: User,
  ) {
    super._preUpdate(changed, options, user);
    //return early if the parent isn't an actor or we're not actually affecting items
    if (
      this.affectsItems &&
      this.parent instanceof CONFIG.Actor.documentClass
    ) {
      this._removeEffectsFromItems(this.parent);
    }
  }

  protected override async _preDelete(
    options: DocumentModificationOptions,
    user: User,
  ) {
    super._preDelete(options, user);
    const parent = this.parent;
    //remove the effects from the item
    if (this.affectsItems && parent instanceof CONFIG.Actor.documentClass) {
      this._removeEffectsFromItems(parent);
    }
    // Get the active Combat if there is one.
    const activeCombat = game.combats?.active;
    if (activeCombat) {
      // Get the Combatant that corresponds to the Actor.
      const combatant = activeCombat.getCombatantByActor(
        this.parent?.id as string,
      );
      // If there is a corresponding Combatant, process Combatant Controls
      if (combatant) {
        // If status is Holding, turn off Hold for Combatant.
        this.statusId;
        if (this.statusId === 'holding') {
          await combatant?.unsetFlag('swade', 'roundHeld');
        }
      }
    }
  }

  protected override async _preCreate(
    data: ActiveEffectDataConstructorData,
    options: DocumentModificationOptions,
    user: BaseUser,
  ): Promise<void> {
    super._preCreate(data, options, user);
    if (!data.icon) {
      this.updateSource({
        icon: 'systems/swade/assets/icons/active-effect.svg',
      });
    }
    // Get the active Combat if there is one.
    const activeCombat = game.combats?.active;
    if (activeCombat) {
      // Get the Combatant that corresponds to the Actor.
      const combatant = activeCombat.getCombatantByActor(
        this.parent?.id as string,
      );
      // If there is a corresponding Combatant, process Combatant Controls
      if (combatant) {
        // If status is Holding, turn on Hold for Combatant.
        if (this.statusId === 'holding') {
          await combatant.setRoundHeld(activeCombat.current.round as number);
        }
      }
    }

    //localize labels, just to be sure
    this.updateSource({ label: game.i18n.localize(this.label) });

    //automatically favorite status effects
    if (data.flags?.core?.statusId) {
      this.updateSource({ 'flags.swade.favorite': true });
    }

    // If there's no duration value and there's a combat, at least set the combat ID which then sets a startRound and startTurn, too.
    if (!data.duration?.combat && game.combat) {
      this.updateSource({ 'duration.combat': game.combat.id });
    }

    //set the world time at creation
    this.updateSource({ duration: { startTime: game.time.worldTime } });

    if (this.getFlag('swade', 'loseTurnOnHold')) {
      const combatant = game.combat?.combatants.find(
        (c) => c.actor?.id === this.parent?.id,
      );
      if (combatant?.getFlag('swade', 'roundHeld')) {
        await Promise.all([
          combatant?.setFlag('swade', 'turnLost', true),
          combatant?.unsetFlag('swade', 'roundHeld'),
        ]);
      }
    }
  }

  protected override _onCreate(
    data: PropertiesToSource<ActiveEffectDataProperties>,
    options: DocumentModificationOptions,
    userId: string,
  ): void {
    super._onCreate(data, options, userId);
    const relatedEffects = this.getFlag('swade', 'related');
    if (relatedEffects && this.parent?.documentName === 'Actor') {
      const isStatusEffect = !!this.statusId;
      for (const [id, mutation] of Object.entries(relatedEffects)) {
        const statusEffect = CONFIG.statusEffects.find((v) => v.id === id);
        if (!statusEffect) continue; //skip if we can't find the effect
        //apply the mutation if one exists
        const effect = foundry.utils.isEmpty(mutation)
          ? statusEffect
          : foundry.utils.mergeObject(statusEffect, mutation, {
              performDeletions: true,
            });
        setProperty(effect, 'flags.swade.favorite', isStatusEffect);
        this.parent.toggleActiveEffect(effect, { active: true });
      }
    }
  }
}
